@tool
extends Node2D

## Not sure of this is intended, but the reference count for the
## resource "ares.tres" keeps climbing everytime you view it in
## the inspector.
## Focus on the Node2D and then click the tog bool in the inspector
## to see the output.
## I rather expected the ref count to not keep climbing.

@export var tog:bool:
	set(b):
		test()

var myres:Resource = load("res://ares.tres")

func test():
	info()
	call_deferred("inspect") # if you don't do this, godot crashes..

func inspect():
	#EditorInterface.edit_resource(myres) # either of these
	EditorInterface.inspect_object(myres) # two does the same thing

	# Refocus on the node2d so you can keep clicking.
	# I had hoped this would also unref myres, but seems not.
	EditorInterface.inspect_object($".")

func info():
	print("-- myres info --")
	myres.report()
